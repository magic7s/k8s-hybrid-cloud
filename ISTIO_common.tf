// Common configurations for Istio
variable ISTIO_helm_yaml_url {}

variable ISTIO_chart_repo {}
variable ISTIO_chart_repo_name {}
variable ISTIO_version {}

locals {
  control_cluster_kubeconfig = "./kubeconfig_${google_container_cluster.primary.name}"
  remote_cluster_kubeconfig  = "./kubeconfig_${var.EKS_name}"
}
