provider "aws" {
  region = "${var.AWS_region}"
  version = "2.15"
}

provider "google" {
  credentials = "${ file(var.GCP_credentials) }"
  project     = "${ var.GCP_project }"
  region      = "${ var.GCP_region }"
  version     = "2.8"
}

provider "external" {
    version = "1.1"
}

provider "local" {
    version = "1.2"
}

provider "null" {
    version = "2.1"
}

provider "template" {
    version = "2.1"
}

provider "helm" {
 #alias  = "gke"
  version = "0.10.0"
  service_account = "tiller"
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v2.11.0"

  kubernetes {
    config_path = "${ local.control_cluster_kubeconfig }"
  }
}

provider "helm" {
  version = "0.10.0"  
  alias           = "eks"
  service_account = "tiller"
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v2.11.0"

  kubernetes {
    config_path = "${ local.remote_cluster_kubeconfig }"
  }
}
